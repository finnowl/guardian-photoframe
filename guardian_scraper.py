 

import requests
#import urllib.request
from bs4 import BeautifulSoup

#import pprint
from datetime import datetime
import time
import random
import re

import sqlite3
from os import path



base_url = "https://www.theguardian.com/news/series/ten-best-photographs-of-the-day"

page = requests.get(base_url)
soup = BeautifulSoup(page.text, "html.parser")


article_hrefs = soup.find_all("a", {"data-link-name": "article"}) #daily articles raw


hrefs = [] #daily articles cleaned hrefs

#clean hrefs
for href_raw in article_hrefs:

	href = href_raw["href"]

	if href not in hrefs: #add only if truly new
		hrefs.append(href)


#OPEN DATABASE
ROOT = path.dirname(path.realpath(__file__)) #for pythonanywhere, but works locally aswell
conn = sqlite3.connect(path.join(ROOT, "guardian_scraper.db"))
cur = conn.cursor()


update_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S') #get date and time


hrefs_refined = [] #the hrefs that are new in the database


#ADD TO DATABASE (TO TABLE: collections) AND EVALUATE WHETHER NEW HREF
for href in hrefs:

	try:

		li_sql = """INSERT INTO collections(collection_url, collection_added) VALUES(?, ?);"""
		cur.execute(li_sql, (href, update_date))
		conn.commit()

		hrefs_refined.append(href) #add to list that will be used for second scraping

		print("ADDED COLLECTION:\n{}\n".format(href))

	except:

		print("NOT ADDED COLLECTION:\n{}\n".format(href))


#print(hrefs_refined)


photo_data = [] #final photo data


#CHECK EACH DAILY PAGE AND GET ALL PHOTO DATA
for href in hrefs_refined:

	page1 = requests.get(href, 'html.parser')

	image_hrefs = re.findall(r"sets\":\"(https\S*width=1920&quality\S*)\s", page1.text)

	image_captions = re.findall(r"\"caption\":\"(.*?)\",\"credit", page1.text)

	image_credits = re.findall(r"\"credit\":\"(.*?)\",\"displayCredit", page1.text)

	time.sleep(	random.randint(8, 17)/10) #important not to overload, wait 0.8-1.7 sec to emulate human


	#split lists above into dictionary for each photo
	for item in range(0, len(image_hrefs)):

		photo_data.append({"image_href": image_hrefs[item], "image_caption": image_captions[item], "image_credits": image_credits[item]})


#CHECK IF CAPTIONS END WITH A DOT
for photo in photo_data:
	if not photo["image_caption"].endswith("."): #if false
		photo["image_caption"] = "{}.".format(photo["image_caption"])


#print(photo_data)


#ADD TO DATABASE (TO TABLE: photos)
for photo in photo_data:

	try:

		if photo["image_href"].startswith("http"): #check if href is a real url

			li_sql = """INSERT INTO photos(photo_url, photo_added, photo_caption, photo_credits) VALUES(?, ?, ?, ?);"""
			cur.execute(li_sql, (photo["image_href"], update_date, photo["image_caption"], photo["image_credits"]))
			conn.commit()

			print("ADDED PHOTO:\n{}\n".format(photo["image_href"]))

	except:

		print("ERROR WITH PHOTO:\n{}\n".format(photo["image_href"]))


#CLOSE DATABASE
cur.close()
conn.close()





'''


	for i in range(0, len(image_hrefs)):

		print("<li class='slide' onclick='myFunction()' style='background-image: url(\"{}\")' caption='{}' credits='{}'></li>".format(image_hrefs[i], image_captions[i], image_credits[i]))
		print("")

'''

