# Scraper that updates a photo frame based on the Guardian blog

This scraper, running hourly on pythonanywhere.com, updates a SQLite database of photos featured in the marvelous blog 'Ten Best Photographs of the Day' by [the Guardian](https://www.theguardian.com/news/series/ten-best-photographs-of-the-day).

A Flask script then dynamically creates a [webpage](https://finnowl.pythonanywhere.com/photo_frame) that showcases the most recent photos as a fullscreen slideshow.

Created in May 2022.
