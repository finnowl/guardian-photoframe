def guardian_get_from_database():


	import requests
	import json
	import re
	from os import path
	import sqlite3

	ROOT = path.dirname(path.realpath(__file__)) #for pythonanywhere, but works locally aswell
	conn = sqlite3.connect(path.join(ROOT, "guardian_scraper.db"))

	#open database
	conn.row_factory = sqlite3.Row #access results like dictionary
	cur = conn.cursor()


	li_sql = """SELECT photo_url, photo_caption, photo_credits FROM photos ORDER BY photo_id DESC LIMIT 40;""" #get 40 newest based in id
	all_programs = cur.execute(li_sql)
	all_programs_list = cur.fetchall() #can be accessed via dict notation

	all_programs_dicts = []

	#CONVERT SQLLITE3 TO DICT
	for i in range(0, len(all_programs_list)):
		all_programs_dicts.append({"photo_url": all_programs_list[i]["photo_url"], "photo_caption": all_programs_list[i]["photo_caption"], "photo_credits": all_programs_list[i]["photo_credits"]})

	#close database
	cur.close()
	conn.close()


	first_li = """<li class='slide active' onclick='myFunction()' style='background-image: url("{}")' caption='{}' credits='{}'></li>""".format(all_programs_dicts[0]["photo_url"], all_programs_dicts[0]["photo_caption"], all_programs_dicts[0]["photo_credits"])


	rest_of_lis = ""

	for item in all_programs_dicts[1:]: #strip first element in list

		rest_of_lis += """<li class='slide' onclick='myFunction()' style='background-image: url("{}")' caption='{}' credits='{}'></li>\n\n""".format(item["photo_url"], item["photo_caption"], item["photo_credits"])


	first_caption_credits = "{} <i>{}</i>".format(all_programs_dicts[0]["photo_caption"], all_programs_dicts[0]["photo_credits"])


	html_template = """
		<!doctype html>
		<html class="no-js" lang="">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<title></title>
			<meta name="description" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name=”apple-mobile-web-app-capable” content=”yes”>

			<link rel="manifest" href="site.webmanifest">
			<link rel="apple-touch-icon" href="icon.png">

			<!-- Place favicon.ico in the root directory -->
			<script src="https://kit.fontawesome.com/f9b4fb9c40.js" crossorigin="anonymous"></script>

			<link rel="stylesheet" href="style.css">
		</head>

		<body>

			<div class="container">
				<ul id="all_slides">

					{}

					{}

				</ul>

				<div id="explanation">

					{}

				</div>


				<div class="buttons">
					<button class="controls" id="previous"><i class="far fa-arrow-alt-circle-left"></i></button>
					<button class="controls" id="pause"><i class="far fa-pause-circle"></i></button>
					<button class="controls" id="next"><i class="far fa-arrow-alt-circle-right"></i></button>
				</div>

			</div>

			<script src="main.js"></script>

		</body>
		</html>

	""".format(first_li, rest_of_lis, first_caption_credits)


	#print(html_template)

	return(html_template)

if __name__ == "__main__":
    guardian_get_from_database()